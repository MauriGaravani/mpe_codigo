#include <Arduino.h>
#include <Keypad.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

#define ENTRADA_PIR 2 //Macros
#define VALVULA 3
#define LEDVERDE 4
#define LEDROJO 5
#define CONTADOR 11

int tiempo = 2000; //Variables
float pulsos; 
float duracion = 0;
float duraciont = 0;
bool tecla1 = 0;
bool estado = 0; 

void interrupcion_pir (void); //Prototipado
void interrupcion1_pir (void); 

LiquidCrystal_I2C lcd(0x3F,16,2); // Conexion en pines A4 y A5

const byte numRows= 1; // Tiene 1 fila
const byte numCols= 3; // Tiene 3 columnas

char keymap[numRows][numCols]= 
{
{'1', '2', '3','A'}
};

byte rowPins[numRows] = {10}; // Estos terminales del Arduino corresponden a Filas
byte colPins[numCols]= {6,7,8,9}; // Estos terminales del Arduino corresponden a Columnas

Keypad myKeypad= Keypad(makeKeymap(keymap), rowPins, colPins, numRows, numCols);