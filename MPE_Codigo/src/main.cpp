#include <Arduino.h>
#include <Keypad.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include <Interrupt_Pir.h>

void setup() {
  pinMode(ENTRADA_PIR, INPUT); //Declaramos la entrada y salida de pines
  pinMode(LEDVERDE, OUTPUT);
  pinMode(LEDROJO, OUTPUT);
  pinMode(VALVULA, OUTPUT);
  pinMode(CONTADOR, OUTPUT); 
  Serial.begin(9600); //Comunicacion serie iniciada a 9600 baudios
  lcd.init();
  attachInterrupt(digitalPinToInterrupt(ENTRADA_PIR), interrupcion_pir, RISING); //Interrupcion flanco ascendente
  attachInterrupt(digitalPinToInterrupt(ENTRADA_PIR), interrupcion1_pir, FALLING); //Interrupcion flanco descendente
}

void loop() {
  char pulsacion = Teclado1.getKey() ;
  pulsos = pulseIn(ENTRADA_PIR, LOW);
  duracion = (pulsos/1000000);
  duraciont = (duracion + duraciont);

   //         TECLA 1         //  
 if (pulsacion == '1')
 {
   if(tecla1 == 0)
   {
   digitalWrite(LEDROJO, LOW);
   digitalWrite(LEDVERDE, HIGH);
   lcd.backlight();
   lcd.setCursor(0, 0);
   lcd.print("Bienvenidos al  ");
   lcd.setCursor(0, 1);
   lcd.print("programa        ");   
   delay(1500);
   lcd.setCursor(0, 0);
   lcd.print("1-Apagar 2-ModoA");
   lcd.setCursor(0, 1);
   lcd.print("3-Tiempo A-Menu ");   
   tecla1 = 1;  
   }
  else
  {
   digitalWrite(LEDVERDE, LOW);
   digitalWrite(LEDROJO, HIGH);
   lcd.clear();
   lcd.noBacklight();
   tecla1 = 0;
  }
 }
 //         TECLA 2         //
 if (pulsacion == '2')
 {
   lcd.setCursor(0, 0);
   lcd.print("Modo Ahorro     ");
   lcd.setCursor(0, 1);
   lcd.print("Iniciado        ");  
   tiempo = 500;
   delay(1000);
   lcd.setCursor(0, 0);
   lcd.print("Presione A para");
   lcd.setCursor(0, 1);
   lcd.print("regresar al menu"); 
 }
 //         TECLA 3         //
 if (pulsacion == '3')
 {
   lcd.setCursor(0, 0);
   lcd.print("Contador tiempo ");
   lcd.setCursor(0, 1);
   lcd.print("Activado        ");
   delay (100);
   digitalWrite(CONTADOR, HIGH);  
   delay(1000);
   lcd.setCursor(0, 0);
   lcd.print("Presione A para");
   lcd.setCursor(0, 1);
   lcd.print("regresar al menu"); 
 }
   //         TECLA A         //
 if (pulsacion == 'A')
 {
   lcd.setCursor(0, 0);
   lcd.print("1-Apagar 2-ModoA");
   lcd.setCursor(0, 1);
   lcd.print("3-Tiempo A-Menu ");
 }
 //         Funcionamiento         //
 if (tecla1 == 1)
 {
   digitalWrite(VALVULA, estado);  
 }
 
}